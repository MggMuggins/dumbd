FROM centos:latest

WORKDIR /usr/local/bin
COPY dumbd ./dumbd

RUN chmod uga+x /usr/local/bin/dumbd

ENTRYPOINT ["/usr/local/bin/dumbd"]

